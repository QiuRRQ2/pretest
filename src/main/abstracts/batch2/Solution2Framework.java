package main.abstracts.batch2;

/**
 * @author Erick Pranata
 * @since 2019-06-24
 */
public interface Solution2Framework {
    int solution(Integer[] input);
}
