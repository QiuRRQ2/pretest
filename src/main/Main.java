package main;

import main.abstracts.ExampleFramework;
import main.solutions.ExampleSolution;

public class Main {

    public static void main(String[] args) {
        ExampleFramework exampleSolution = new ExampleSolution();
        int[] exampleInput = new int[] { 2, 6, 3, 6, 8 };
        int exampleOutput = exampleSolution.solution(exampleInput);
        System.out.println("Example Solution: " + exampleOutput);

//        Solution1Framework solution1 = null;
//        Solution1Framework.IntList solution1Input = null;
//        int solution1Output = solution1.solution(solution1Input);
//        System.out.println("Solution 1: " + solution1Output);

//        Solution2Framework solution2 = null;
//        Integer[] solution2Input = null;
//        int solution2Output = solution2.solution(solution2Input);
//        System.out.println("Solution 2: " + solution2Output);

//        Solution3Framework solution3 = null;
//        Integer[] solution3Input = null;
//        int solution3Output = solution3.solution(solution3Input);
//        System.out.println("Solution 3: " + solution3Output);
    }
}
